LSMASHSourceのビルド備忘録
==========================
忘れると同じことやり直すはめになるのでメモしときます。

準備
----

* MSYS2
* Microsoft Visual Studio 2015 Community Edition  
* ffmpeg (2015/11/16現在のrefs/tags/n2.8.1)  
* L-SMASH (2015/11/16現在のmasterブランチ)  
* L-SMASH-Works (2015/11/16現在のmasterブランチ)  

* pacmanで必要なパッケージをインストール  
``pacman -S make autoconf autogen git perl yasm mingw-w64-i686-gcc mingw-w64-i686-gettext mingw-w64-i686-libtool``  
* MSVCのlink.exeとMSYS2のlink.exeが重複してしまうので名前を変更します。  
  ``mv /usr/bin/link.exe /usr/bin/link.exe.bak``  

以下、MSYS2インストール場所をD:\Terminal、HOMEディレクトリをD:\Terminal\home\amane、  
成果物インストールディレクトリをそれぞれ/mingw32/local、/mingw32/local-msvcとする。

ffmpegのビルド
--------------
* HOMEディレクトリへ移動してffmpegをgitリポジトリからcloneする。  
  ``git clone git://source.ffmpeg.org/ffmpeg.git``  
  ``cd ffmpeg``
* 2015年11月16日現在の最新安定版(n2.8.1)へチェックアウトする。  
  **LSMASH-Worksの更新やffmpegのmasterブランチ更新によってビルド出来ない可能性があるので随時確認すること。**  
  ``git checkout -b v2.8.1 refs/tags/n2.8.1``
* configure  
  ``mkdir build32 && cd build32``  
  ``../configure --prefix=/mingw32/local --extra-ldflags='-static' --target-os=mingw32 --arch=i686 --enable-avresample --disable-programs --disable-doc --disable-swresample --disable-encoders --disable-hwaccels --disable-filters --disable-debug``  
  特記オプション：  
  `--disable-programs` : ffmpeg、ffprobe、ffplayなどのプログラム類のビルドを無効にします。  
  `--disable-encoders` : LSMASHSourceではエンコード機能は使わないので無効にします。  
  `--disable-filters` : 上記と同じ理由で無効にします。  
  `--target-os=mingw32` : mingw32ビルドなので指定。  
  `--arch=i686` : 32ビットでビルドするのでi686、64ビットならx86_64を指定します。  
  他のオプションは適宜見直しつつ設定してください。  
  *2015.11.19確認*  
  **このオプションではAviUtlプラグインが動かない可能性があります。**  

* makeとインストール  
  ``make && make install``

L-SMASHのビルド
---------------
* L-SMASHのリポジトリをcloneする。  
  ``cd ~``  
  ``git clone git://github.com/l-smash/l-smash.git``  
  ``cd l-smash``  
* configure  
  ``mkdir build32 && cd build32``  
  ``../configure --prefix=/mingw32/local --extra-ldflags='static'``  
* makeとインストール  
  ``make && make install``  

L-SMASH Works (AviUtlとVapourSynthのプラグイン)のビルド
-------------------------------------------------------
* L-SMASH Worksのリポジトリをcloneする。  
  ``cd ~``  
  ``git clone git://github.com/VFR-maniac/L-SMASH-Works.git``  

### AviUtlプラグイン ###
* configure  
  L-SMASHのインストール場所が/mingw32/localだったので**PKG__CONFIG__PATH**を指定します。  
  ``cd L-SMASH-Works/AviUtl``  
  ``mkdir build32``
  ``PKG_CONFIG_PATH=/mingw32/local/lib/pkgconfig ./configure --prefix=/mingw32/local --extra-ldflags='-static'``
* make  
  ``make``  
  ``mv lwcolor.auc build32 && mv lwmuxer.auf build32 && mv lwdumper.auf build32 && mv lwinput.aui build32``  
  ``make distclean``
### Vapoursynthプラグイン ###
* configure  
  L-SMASHのインストール場所が/mingw32/localだったので**PKG__CONFIG__PATH**を指定します。  
  ``cd ../VapourSynth``  
  ``mkdir build32``  
  ``PKG_CONFIG_PATH=/mingw32/local/lib/pkgconfig ./configure --prefix=/mingw32/local --extra-ldflags='-static'``  
* make  
  ``make``  
  ``mv vslsmashsource.dll build32``  
  ``make distclean``  

AviSynth関連のビルド
--------------------
AviSynth関連のプラグインはライブラリの関係上、Microsoft Visual C++でビルドします。  
このため、ffmpeg・L-SMASH・L-SMASH-Worksはすべてgcc以外でビルドする必要があります。  

### 最初に ###
1. スタートメニューから**Visual Studio 2015 -> VS2015 x86 Native Tools コマンドプロンプト**を実行してコマンドプロンプトを起動します。  
2. 次のコマンドを実行してMSYS2のターミナルへ移動します。  
  ``D:\Terminal\mingw32_shell.bat``
### ffmpegのビルド ###
* configure  
  ``cd ffmpeg``  
  ``mkdir build32-msvc && cd build32-msvc``  
  ``../configure --toolchain=msvc --prefix=/mingw32/local-msvc --enable-avresample --disable-programs --disable-doc --disable-swresample --disable-encoders --disable-hwaccels --disable-filters --disable-debug``  
  **configure結果が以下のようになっているのを確認すること。**

--------------------------
    source path   /home/amane/ffmpeg
    C compiler    clj
    C library     msvcrt
    ARCH          x86 (generic)
    big-endian    no

* make  
  警告が文字化けしますが気にしないでください。  
  ``make && make install``  
### L-SMASHのビルド ###
ここからはVisual Studio 2015での作業となります。  

1. L-SMASH.slnをVisual Studio 2015で開く。  
2. 「VC++ コンパイラをアップグレードします」と出るのでOKをクリックする。  
3. 「ソリューション'L-SMASH'」を右クリックして構成マネージャー  
  「アクティブ ソリューション構成」をReleaseへ変更する。  
4. プロジェクト liblsmashを右クリック、プロパティ
  「構成プロパティ」の「全般」で  
  プロジェクトの既定値->構成の種類を「ダイナミックリンクライブラリ」から「スタティックライブラリ」へ変更する。  
5. 「ビルド(B)」からソリューションをビルドします。  
  以下のような警告が出ますが気にしないでください。  

---------------------------
    warning C4244: '関数': 'uint64_t' から 'uint32_t' への変換です。データが失われる可能性があります。
    warning C4244: '=': 'int64_t' から 'int' への変換です。データが失われる可能性があります。

6. Releaseディレクトリの中にliblsmash.libファイルが出来上がるのでこのファイルを  
  /mingw32/local-msvc/libフォルダへコピーする。  
7. l-smashフォルダの_lsmash.h_ファイルを/mingw32/local-msvc/includeフォルダへコピーする。  
### L-SMASH-Works (AviSynthプラグイン)のビルド ###
1. L-SMASH-Works/AviSynth/LSMASHSourceVCX.slnをVisual Studio 2015で開く。  
2. 「VC++ コンパイラをアップグレードします」と出るのでOKをクリックする。  
3. 「ソリューション'LSMASHSourceVCX'」を右クリックして構成マネージャー  
  「アクティブ ソリューション構成」を**Release**へ変更する。  
4. プロジェクト LSMASHSourceを右クリック、プロパティ  
  「VC++ ディレクトリ」->全般のインクルードディレクトリに以下のディレクトリを追加する。

--------------------
    D:\Terminal\mingw32\local-msvc\include

5. ライブラリディレクトリに以下のディレクトリを追加する。  

--------------------
    D:\Terminal\mingw32\local-msvc\lib

ここからは他の設定です。  

* 「C/C++」->「全般」でデバッグ情報の形式を「なし」にする。  
* 「リンカー」->「入力」で特定の既定のライブラリの無視に「libcmt.lib」を追加する。  
* 「リンカー」->「デバッグ」でデバッグ情報の生成を「いいえ」にする。  
* 「リンカー」->「最適化」で参照を「いいえ(/OPT:NOREF)」にする。

### ソースコードの修正 ###
このままではリテラルやリンク関係でエラーが出るのでソースコードを書き換えます。  
ネットではinttypes.hとstdint.hをどこかインクルードパスが通った場所へコピーするのが一般的ですが
インストール環境を壊したくないため、ソースコードを直接書き換えます。  
(どうやらC99という規格らしいのですがよく分からないのでこの辺りは適当に)

コンパイルエラーが出るのは以下のソースコードなのでこのファイルを書き換えます。  

    libavsmash_video.c
    lwindex.c

まずは  
``error C3688: リテラル サフィックス 'PRIu32' が無効です。リテラル演算子またはリテラル演算子テンプレート 'operator ""PRIu32' が見つかりません``  
というエラーが出ている場所を見てみます。  

    if( duration == 0 )
        {
            lsmash_delete_media_timestamps( &ts_list );
            if( lhp->show_log )
                lhp->show_log( lhp, LW_LOG_WARNING, "Detected CTS duplication at frame %"PRIu32, i );
            return 0;
        }

どうやらPRIu32という場所でエラーが出ているみたいです。ここを  

    if( duration == 0 )
        {
            lsmash_delete_media_timestamps( &ts_list );
            if( lhp->show_log )
                lhp->show_log( lhp, LW_LOG_WARNING, "Detected CTS duplication at frame %u", i );
            return 0;
        }

としておきましょう。  
以下、置き換えるべきものを載せておきます。

    PRIu32 -> "u"
    PRId64 -> "lld"
    PRIx64 -> "llx"
    SCNd64 -> "lld"
    SCNx64 -> "llx"

次はexlibs.cppを開いてソースコードを以下のように変更します。  

    //#pragma comment( lib, "libmingwex.a" )  //コメントアウト
    //#pragma comment( lib, "libgcc.a" )      //コメントアウト
    #pragma comment( lib, "liblsmash.lib" )   //.aから.libへ変更
    #pragma comment( lib, "libavutil.a" )     //libavutil.aへ変更
    #pragma comment( lib, "libavcodec.a" )    //libavcodec.aへ変更
    #pragma comment( lib, "libavformat.a" )   //libavformat.aへ変更
    #pragma comment( lib, "libswscale.a" )    //libswscale.aへ変更
    #pragma comment( lib, "libavresample.a" ) //libavresample.aへ変更
    #pragma comment( lib, "ws2_32.lib" )      //追加

これでビルド可能になりました。「ビルド(B)」->ソリューションのビルドを実行して  
Releaseディレクトリ内にLSMASHSource.dllができていれば成功です。




参考元
------
1. [How to compile L-SMASH and L-SMASH Works with Visual Studio 2013](https://github.com/BrunoReX/build-scripts/blob/master/L-SMASH/readme.md)
    * 2015/11/16現在
